import csv
import os

import boto3
from celery import Celery
from flask import Flask, Response, redirect, render_template, request, url_for

from models import Product, db

app = Flask(__name__)

DATABASE_URL = os.environ.get('DATABASE_URL', 'postgresql://localhost/fulfil_products')
UPLOAD_FOLDER = '/uploads'
ALLOWED_EXTENSIONS = set(['csv'])

app.config['CELERY_BROKER_URL'] = os.environ.get('REDIS_URL', 'redis://localhost:6379')
app.config['CELERY_RESULT_BACKEND'] = os.environ.get('REDIS_URL', 'redis://localhost:6379')
app.config['SQLALCHEMY_DATABASE_URI'] = DATABASE_URL
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 100 * 1024 * 1024

celery = Celery(app.import_name, backend=app.config['CELERY_RESULT_BACKEND'],
                    broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)

with app.app_context():
    db.init_app(app)
    db.create_all()


@app.route('/', methods=['GET'])
def index():
    return redirect("/products", code=302)

@app.route('/products', methods=['GET'])
def products():
    search = request.args.get('search', '')
    active_page = int(request.args.get('actpage', 1))
    inactive_page = int(request.args.get('inactpage', 1))
    page_size = 1000

    if search:
        total_active_products = Product.query \
                            .filter((Product.sku.contains(search) | Product.name.contains(search)) & Product.is_active) \
                            .count()
        active_products = Product.query \
                            .filter((Product.sku.contains(search) | Product.name.contains(search)) & Product.is_active) \
                            .order_by(Product.id) \
                            .paginate(active_page, page_size, False)
        total_inactive_products = Product.query \
                            .filter((Product.sku.contains(search) | Product.name.contains(search)) & ~Product.is_active) \
                            .count()
        inactive_products = Product.query \
                            .filter((Product.sku.contains(search) | Product.name.contains(search)) & ~Product.is_active) \
                            .paginate(inactive_page, page_size, False)
    else:
        total_active_products = Product.query \
                            .filter_by(is_active=True) \
                            .count()
        active_products = Product.query \
                            .filter_by(is_active=True) \
                            .order_by(Product.id) \
                            .paginate(active_page, page_size, False)
        total_inactive_products = Product.query \
                            .filter_by(is_active=False) \
                            .count()
        inactive_products = Product.query \
                            .filter_by(is_active=False) \
                            .order_by(Product.id) \
                            .paginate(inactive_page, page_size, False)

    active_next_url = None
    active_prev_url = None
    inactive_next_url = None
    inactive_prev_url = None

    if active_products.has_next:
        active_next_url = url_for('products', actpage=active_products.next_num, inactpage=inactive_page, search=search)
    if active_products.has_prev:
        active_prev_url = url_for('products', actpage=active_products.prev_num, inactpage=inactive_page, search=search)
    if inactive_products.has_next:
        inactive_next_url = url_for('products', actpage=active_page, inactpage=inactive_products.next_num, search=search)
    if inactive_products.has_prev:
        inactive_prev_url = url_for('products', actpage=active_page, inactpage=inactive_products.prev_num, search=search)

    context = {
        'title': 'List Products',
        'active_page': 'list',
        'search': search,
        'active_products': active_products.items,
        'inactive_products': inactive_products.items,
        'total_active_products': total_active_products,
        'total_inactive_products': total_inactive_products,
        'curr_active_page': active_page,
        'curr_inactive_page': inactive_page,
        'active_next_url': active_next_url,
        'active_prev_url': active_prev_url,
        'inactive_next_url': inactive_next_url,
        'inactive_prev_url': inactive_prev_url,
    }
    # if page:
    #     context['page'] = page
    return render_template('index.html', **context)

@app.route('/import', methods=['GET', 'POST'])
def import_page():
    context = {
        'title': 'Import Products',
        'active_page': 'import'
    }
    return render_template('import.html', **context)

@celery.task()
def process_giant_list(file_details):
    print('Starting file processing')
    with app.app_context():
        csv_input = False
        try:
            s3 = boto3.client('s3')
            uploaded_file = s3.get_object(Bucket=file_details.get('Bucket'), Key=file_details.get('Key'))
            csv_input = list(csv.reader(uploaded_file['Body'].read().decode("UTF8").splitlines(True)))

            print('File downloaded successfully')
        except Exception as e:
            print('File download failed')
            return False
        success_count = 0
        failure_count = 0
        if csv_input:
            for idx, row in enumerate(csv_input[1:]):
                try:
                    product = Product(row[1], row[0], row[2], True)
                    db.session.add(product)
                    db.session.commit()

                    success_count += 1
                except Exception as e:
                    db.session.rollback()
                    failure_count += 1
                    pass
            print('All records processed successfully. Success Count: {}, Failure Count: {}'.format(success_count, failure_count))
            return True
        else:
            print('CSV File has no records')
            return False

@app.route('/import/file', methods=['POST'])
def import_file():
    try:
        process_giant_list.delay({'Bucket': request.form.get('Bucket'), 'Key': request.form.get('Key')})
    except Exception as e:
        return Response({}, status=400)
    return Response({}, status=200)

@app.route('/add', methods=['GET', 'POST'])
def add_data():
    if request.method == 'GET':
        context = {
            'title': 'Add Product',
            'active_page': 'add'
        }
        return render_template('add.html', **context)
    if request.method == 'POST':
        try:
            product = Product(request.form.get('sku'), request.form.get('name'), request.form.get('description', ''), request.form.get('is_active', False))
            db.session.add(product)
            db.session.commit()
            context = {
                'title': 'Add Product',
                'active_page': 'add',
                'success': True,
                'message': 'Product Added Successfully'
            }
        except Exception as e:
            context = {
                'title': 'Add Product',
                'active_page': 'add',
                'success': False,
                'message': 'Product not added'
            }
        return render_template('add.html', **context)


if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port, debug=True)
