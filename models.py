from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Product(db.Model):
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    sku = db.Column(db.String(100), unique=True, nullable=False)
    name = db.Column(db.String(100), unique=False, nullable=False)
    description = db.Column(db.String(500), unique=False, nullable=True)
    is_active = db.Column(db.Boolean)

    def __init__(self, sku, name, description, is_active, **kwargs):
        self.sku = sku
        self.name = name
        self.description = description
        self.is_active = bool(is_active)
        super(Product, self).__init__(**kwargs)

    def __repr__(self):
        return str(self.name)
